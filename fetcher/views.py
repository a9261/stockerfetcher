from django.shortcuts import render
from django.http import HttpResponse,JsonResponse
import twstock
import json
# Create your views here.
def index(request):
    stockNo = str(request.GET.get('stockno'))
    if stockNo=="None":
        return HttpResponse("Please input stockNo")
    
    stock = twstock.Stock(stockNo)
    return HttpResponse("Hello"+stock.sid)

def getRealTimeInfo(request):
    stockNo = str(request.GET.get('stockno'))
    
    if stockNo=="None":
        return HttpResponse("Please input stockNo")

    info = twstock.realtime.get(stockNo)    
    return JsonResponse(info)

def getBuyInfo(request):
    stockNo = str(request.GET.get('stockno'))
   
    if stockNo=="None":
        return HttpResponse("Please input stockNo")
    
    stock = twstock.Stock(stockNo)
    bfp = twstock.BestFourPoint(stock)
    result = bfp.best_four_point_to_buy()
    model = BetsFourPointModel()
    dic = {}
    if(not result):
        dic['ShouldBuy']=True
        # dic['BuyMessage']=result.__dict__
        # model.ShouldBuy=True
        # model.BuyMessage=json.dumps(bfp.__dict__)

    jsonResult = json.dumps(model.__dict__ ,ensure_ascii=False)
    return JsonResponse(dic,safe=False)


class BetsFourPointModel:
    def __init__(self):
        self.BuyMessage=""
        self.ShouldBuy=False